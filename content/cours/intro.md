---
title: Ressources d'enseignement
type: blog
---

{{<cours>}}

Nos étudiant·e·s arrivent souvent en cours la tête pleine de diverses erreurs assez élémentaires sur la théorie de l'évolution, issues de la culture populaire ou des retombées de la guerre entre science et religion. Dissiper ces malentendus est parfois difficile et risque de leur donner l'impression d'un échec, de n'avoir pas compris quelque chose d'évident.

Pourtant, en nous penchant sur l'histoire de la biologie, nous découvrons que bien des erreurs formulées par ce public étudiant recoupent celles qui ont caractérisé la période de l'introduction par Darwin de la notion d'évolution. Pour dire les choses simplement, l'idée d'évolution est difficile à maîtriser : les erreurs de nos étudiant·e·s, loin d'être idiotes, sont exactement celles où tombent des scientifiques professionnel·le·s en exercice. Mon objectif ici est de vous fournir quelques supports prêts à l'emploi vous permettant d'intégrer à votre enseignement des exemples de ces erreurs, accompagnés d'extraits pertinents des textes sources.

J'ai conçu cette série de cours pour traiter cinq de ces malentendus à partir d'épisodes historiques réels, en apportant à la fois des connaissances sur la théorie de l'évolution et du contexte historique et en abordant des questionnements sur la nature de la science, angle à l'importance croissante dans la pédagogie actuelle des sciences. Chaque cours suit schématiquement le format adopté par Douglas Allchin dans [_Teaching the Nature of Science: Perspectives & Resources_](https://www.amazon.com/Teaching-Nature-Science-Perspectives-Resources/dp/098925240X), magistral exposé des éclairages que les contenus d'histoire et de philosophie des sciences peuvent fournir dans l'éducation sur la nature des sciences.

Ces cours traiteront des conceptions erronées suivantes :

- L'histoire de l'évolution serait celle d'un progrès continu tendant vers des organismes **plus parfaits** ou **plus complexes**, les êtres humains trônant au sommet de la pyramide.
  - Nous examinerons les tiraillements de **Darwin lui-même** vis-à-vis de cette question : bien qu'il ait déclaré à de multiples reprises qu'il ne fallait pas décrire la marche de l'évolution en termes de progrès, il est régulièrement retombé dans ce type de formulations.
- La théorie de l'évolution ne serait pas **véritablement scientifique** car elle n'utilise pas **la** **méthode scientifique**.
  - Cette critique a été opposée à Darwin lui-même, en particulier par le philosophe du dix-neuvième siècle **John Herschel**.
- Les organismes évolueraient grâce à leurs **efforts pour réussir**, dont ils transmettraient les résultats à leur descendance.
  - Cette vision **lamarckienne** était défendue par un certain nombre de biologistes à la fin du dix-neuvième siècle et au début du vingtième.
- L'évolution ne serait pas toujours liée à l'adaptation mais parfois se retrouverait **bloquée** et génèrerait des traits non adaptatifs.
  - La théorie de l'**orthogénèse**, peut-être plus une curiosité historique qu'une erreur répandue, disait exactement cela. Cette doctrine se révèle une façon intéressante d'approcher les adaptations évolutives et les caractères non adaptatifs.
- Les travaux de génétique de Mendel montreraient que les **caractères sont « binaires »**, soit « a » soit « b », à la manière de ce qu'on met dans un tableau de croisement de Punnett.
  - Même les traits étudiés par Mendel lui-même dans le cas des pois sont plus complexes qu'il n'y paraît à première vue et **W.F.R. Weldon** soutenait aux débuts de la génétique que l'influence de l'environnement était importante.

Dans les leçons qui suivent, vous trouverez de courts exposés historiques contenant des illustrations et des suggestions de lecture. Ils sont ponctués de points réflexion sous forme de questions, pensés pour encourager l'approfondissement de sujets liés à la nature des sciences. Les cours se concluent par une question de réflexion explicitement liée à une problématique de nature des sciences, afin d'encourager la discussion directe sur des sujets majeurs de ce champ pédagogique.

## Téléchargements PDF

- **Cours 1**
  - [Couleur, papier A4](/downloads/Lesson1-Color-A4.fr.pdf)
  - [Couleur, papier _Letter_ américain](/downloads/Lesson1-Color-Letter.fr.pdf)
  - [Noir et blanc, papier A4](/downloads/Lesson1-BW-A4.fr.pdf)
  - [Noir et blanc, papier _Letter_ américain](/downloads/Lesson1-BW-Letter.fr.pdf)
- **Cours 2**
  - [Couleur, papier A4](/downloads/Lesson2-Color-A4.fr.pdf)
  - [Couleur, papier _Letter_ américain](/downloads/Lesson2-Color-Letter.fr.pdf)
  - [Noir et blanc, papier A4](/downloads/Lesson2-BW-A4.fr.pdf)
  - [Noir et blanc, papier _Letter_ américain](/downloads/Lesson2-BW-Letter.fr.pdf)
- **Cours 3**
  - [Couleur, papier A4](/downloads/Lesson3-Color-A4.fr.pdf)
  - [Couleur, papier _Letter_ américain](/downloads/Lesson3-Color-Letter.fr.pdf)
  - [Noir et blanc, papier A4](/downloads/Lesson3-BW-A4.fr.pdf)
  - [Noir et blanc, papier _Letter_ américain](/downloads/Lesson3-BW-Letter.fr.pdf)
- **Cours 4**
  - [Couleur, papier A4](/downloads/Lesson4-Color-A4.fr.pdf)
  - [Couleur, papier _Letter_ américain](/downloads/Lesson4-Color-Letter.fr.pdf)
  - [Noir et blanc, papier A4](/downloads/Lesson4-BW-A4.fr.pdf)
  - [Noir et blanc, papier _Letter_ américain](/downloads/Lesson4-BW-Letter.fr.pdf)
- **Cours 5**
  - [Couleur, papier A4](/downloads/Lesson5-Color-A4.fr.pdf)
  - [Couleur, papier _Letter_ américain](/downloads/Lesson5-Color-Letter.fr.pdf)
  - [Noir et blanc, papier A4](/downloads/Lesson5-BW-A4.fr.pdf)
  - [Noir et blanc, papier _Letter_ américain](/downloads/Lesson5-BW-Letter.fr.pdf)
- **Guide pour enseignant·e·s**
  - [papier A4](/downloads/Guide-A4.fr.pdf)
  - [papier _Letter_ américain](/downloads/Guide-Letter.fr.pdf)

## Remerciements

Un grand merci à [John S. Wilkins](https://evolvingthoughts.net/) pour son aide dans la vérification factuelle des passages historiques de ces cours. Je dois une bonne partie de la structure du cours 5 à l'approche de Greg Radick sur la relation Weldon-Mendel, même si je ne m'imagine pas qu'il soit d'accord avec l'intégralité de ma présentation. Certaines parties de ces contenus remontent à des discussions que j'ai eues sur ces questions avec mes collègues et amis [Greg Macklem](https://stemeducation.nd.edu/directory/faculty/greg-macklem) et [Erik Peterson](https://history.ua.edu/people/erik-peterson/), issues de notre travail pour une présentation au [congrès de 2017 de la National Science Teaching Association à la Nouvelle-Orléans](http://archive.charlespence.net/talks/2017_Darwin_NSTA.pdf). Enfin, mes années d'enseignement au [programme GeauxTeach de la faculté de science de l'université d'État de Louisiane](https://www.lsu.edu/science/geauxteach_sci/) ont influé sur une partie de mes opinions concernant ces questions.

Ces ressources ont été rédigées en partie grâce au financement de l'US National Science Foundation, dans le cadre de la convention de subvention HPS Scholars Award n<sup>o</sup> 1826784.

## Licence

Le texte de ces cours est disponible en libre accès, sous la licence [Creative Commons CC-BY-NC-SA.](https://creativecommons.org/licenses/by-nc-sa/4.0/) Les images, en revanche, sont disponible sur leurs propres licences, qui sont décrites dans les cours eux-mêmes. Notez que la version anglaise n'est pas disponible sous CC-BY-NC-SA, mais sous [CC-BY.](https://creativecommons.org/licenses/by/4.0/) Tous les fichiers sources des cours sont disponibles [chez Codeberg,](https://codeberg.org/cpence/ppa-teaching) avec DOI [10.5281/zenodo.4704737](https://doi.org/10.5281/zenodo.4704737), ou sur l'[UCLouvain OER](https://hdl.handle.net/20.500.12279/802).

## Traduction

Ces cours ont été traduits de l'anglais par [Sandra Mouton,](http://www.sandramouton.com/) et la traduction a été financé par le [Fonds de la Recherche Scientifique - FNRS,](https://www.fnrs.be) subvention n<sup>o</sup> F.4526.19, et le programme « Université numérique » de l'Université catholique de Louvain. Si vous souhaitez revenir à la version anglaise, [cliquez ici.](/lessons/intro)
