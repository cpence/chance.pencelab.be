+++
title = "4. Orthogenesis and “Runaway Evolution”"
+++

{{<lessons>}}
<p>We saw in the last reading that one crucial question for many authors in the early days after the development of Darwin’s theory was the proper explanation of non-adaptive characters. Some of these, arranged into series that seemed to respond to the needs of the organisms’ environments, offered inspiration to the neo-Lamarckians. Another set of examples, the topic of today’s reading, were the basis for another non-Darwinian theory about how evolution might work.</p>
<figure>
<img src="/images/lessons/weismann.jpg" class="smallimg" style="width:33.0%" alt="August Weismann, in 1915 (public domain; Wikimedia Commons)" /><figcaption aria-hidden="true">August Weismann, in 1915 (public domain; Wikimedia Commons)</figcaption>
</figure>
<p>Before we look at the example, though, we should go back to another person we talked about very briefly in the last reading: <a href="https://en.wikipedia.org/wiki/August_Weismann">August Weismann</a> (1834–1914). We mentioned him as having been the first to advance the hypothesis that germ cells are separate from somatic cells, and that germ cells are the only ones involved in the transmission of characters from parents to offspring. But Weismann was equally famous for having proposed an even <a href="https://www.lindahall.org/august-weismann/">stronger version of natural selection than Darwin had.</a> While Darwin argued, as we have already seen, that natural selection was very important, he believed that use and disuse was occasionally a significant factor, and also stressed other processes, some of which we will talk about more below.</p>
<p>Weismann argued that in fact Darwin had not gone far enough. Every single trait, Weismann claimed, was the result of natural selection, from the biggest to the most apparently inconsequential. In an essay in 1883, he wrote that:</p>
<div class="quote-wrapper">
<div class="quote-title">
<p>Weismann, <em>Life and Death</em> (1883)</p>
</div>
<div class="quote" title="Weismann, \emph{Life and Death} (1883)">
<p>The complex world of plants and animals which we see around us contains much that we should call new in comparison with the primitive beings from which, as we believe, everything has developed by means of natural selection.<a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a></p>
</div>
</div>
<div class="think">
<p><strong>THINK[1]:</strong> Can you think of some characters of organisms that might be a problem for this view – characters that don’t seem as though they were produced by natural selection? How might Weismann respond to your examples?</p>
</div>
<p>This view of the power of natural selection was especially problematic for those biologists who had been focused precisely on the importance of characters that seemed like they <em>weren’t</em> adaptive at all. One of these was a German biologist named <a href="https://en.wikipedia.org/wiki/Theodor_Eimer">Theodor Eimer</a> (1843–1898). Eimer’s publications, which had been extremely popular in Germany in the early years after the publication of the <em>Origin,</em> were disseminated worldwide a bit later, as neo-Lamarckism and other kinds of non-Darwinian mechanisms of evolution were embraced more widely.</p>
<p>Eimer’s own theory became known as <em>orthogenesis.</em> Let’s see how he described it, and then unpack what he meant by it.</p>
<div class="quote-wrapper">
<div class="quote-title">
<p>Eimer, <em>On Orthogenesis</em> (1898)</p>
</div>
<div class="quote" title="Eimer, \emph{On Orthogenesis} (1898)">
<p>Definitely directed evolution, orthogenesis, is a universally valid law. It disproves definitively Weismann’s contention of the omnipotence of natural selection – a mere exaggeration of Darwinism and implicitly involving the other view which Weismann has heretofore upheld unconditionally and which Darwin too had once advocated, that all existing characters of animals have some utility. Orthogenesis shows that organisms develop in definite directions without the least regard for utility through purely physiological causes as the result of <em>organic growth,</em> as I term the process. No absolutely injurious character could in the nature of the case continue to exist, but neither could natural selection which Weismann assumes to be the only determining factor in transformation have any efficacy unless something previously existed which from being already useful could be taken hold of by natural selection and so made to serve its purposes.<a href="#fn2" class="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a></p>
</div>
</div>
<p>Eimer’s orthogenesis, then, is the theory that evolution often proceeds down what he calls “definite directions.” Evolution gets stuck, channeled, in particular paths. Whether or not evolution in that direction is good for the organism, it just keeps going. In part, Eimer, argued, this had to do with the way in which variation worked. Orthogenesis would result in a smaller number of available variations, such that going the wrong way, against the movement in the “definite direction” which had already been established within the evolving group, would be impossible.</p>
<div class="think">
<p><strong>THINK[2]:</strong> Imagine that you were asked in 1898 to evaluate Eimer’s theory. What kinds of experiments or observations might you think of as a way to test some of his claims? (Imagine that you both had access to the fossil record and the ability to breed whatever organisms you wanted in whatever kinds of conditions you could think of.)</p>
</div>
<p>If all of this seems very different from natural selection, it is – and Eimer was very clear about what he thought about Darwin’s theory:</p>
<div class="quote-wrapper">
<div class="quote-title">
<p>Eimer, <em>On Orthogenesis</em> (1898)</p>
</div>
<div class="quote" title="Eimer, \emph{On Orthogenesis} (1898)">
<p>The fact that the variations of living beings follow in perfect conformity to law a few definite directions and do not take place accidentally in the most diverse or in all possible directions, alone shatters irreparably the foundation of the Darwinian doctrine. For Darwin’s doctrine must always have a most varied assortment of variations ready at hand if selection is to play a determinative part in the production of forms; and, being a necessary presupposition of the doctrine, this assumption of the constant presence of all possible characters is always asserted as a fact by the champions of omnipotent natural selection.</p>
<p>But if only a few definite tendencies of evolution predominate then they shape the organic world, and there is left for selection only a very subordinate task…<a href="#fn3" class="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a></p>
</div>
</div>
<p>For Eimer, natural selection simply can’t have done any of the things that Darwin says it can – in particular, because orthogenesis constrains the kinds of variation that are available to selection, the process of natural selection will no longer be able to freely create adaptations. On the contrary, it will be doomed to move in whichever directions orthogenesis leaves open for it, whether or not those directions are what we might call adaptive.</p>
<div class="think">
<p><strong>THINK[3]:</strong> We often talk about natural selection as <em>creating</em> adaptations. If we want to understand Eimer’s objection that it cannot, we should first think about what this means. What do you think we mean when we say this? How do natural selection and variation work together to build new features of organisms?</p>
</div>
<p>Even so, Eimer didn’t think that natural selection was totally useless:</p>
<div class="quote-wrapper">
<div class="quote-title">
<p>Eimer, <em>On Orthogenesis</em> (1898)</p>
</div>
<div class="quote" title="Eimer, \emph{On Orthogenesis} (1898)">
<p>I must, in fact, reiterate again and again that natural selection can under no circumstances create anything new. It can only work with existing material, and it cannot even use that until it has attained a certain perfection, until it is already useful. Selection can only remove what is downright injurious, and preserve what is useful. By always selecting the useful it will strengthen its development, but the facts prove that even this can take place only in a restricted measure.<a href="#fn4" class="footnote-ref" id="fnref4" role="doc-noteref"><sup>4</sup></a></p>
</div>
</div>
<p>Natural selection would still have a role, on Eimer’s view. But this would just be to eventually eliminate any organisms that developed in <em>harmful</em> directions. So natural selection works only to remove organisms that don’t work, rather than creating adaptation.</p>
<figure>
<img src="/images/lessons/irish_elk.jpg" class="medimg left" style="width:66.0%" alt="A fossil Irish elk (CC-BY-SA; by Atirador at Wikimedia Commons)" /><figcaption aria-hidden="true">A fossil Irish elk (CC-BY-SA; by Atirador at Wikimedia Commons)</figcaption>
</figure>
<p>All of this will be easier with an example, so let’s look at a spectacular one: the Irish elk. Also known by the name of its genus, <em>Megaloceros,</em> it is notably not an elk, and didn’t only live in Ireland. (Some people call it the “giant deer” instead.) It’s main feature is obvious: giant antlers. The antlers of the Irish elk could grow up to 40 kg (88 lb) in weight, and as large as three and a half meters (12 feet) in width.</p>
<p>We have known about this species for a very long time – the first specimens were described in the 1600s. Obviously no animal like this still existed (at least not in Western Europe, where naturalists had fairly thoroughly cataloged existing species), and so by the 19th century it was described as a particularly famous example of extinction. That leaves a few obvious questions for an evolutionary theorist: what made these deer go extinct? And how could evolution have created something with such exaggerated features in the first place? What purpose would those antlers have served?</p>
<div class="think">
<p><strong>THINK[4]:</strong> What purposes can you think of for which an elk might have used these giant antlers? How other than natural selection do you think they might have come about? How would you compare the various explanations that you’ve thought of?</p>
</div>
<p>Note that almost everyone participating in these arguments assumes that the antlers would <em>not</em> be an adaptation – that is, it would not be useful for the survival of organisms to have antlers that big. Large antlers put them at risk of breaking their necks (whether in a fight or just an accident), and even if they weren’t deadly, they required huge investment in neck bone and muscle that could have been spent on things that would have made them more resistant to predators, for example.</p>
<p>If they weren’t an adaptation, then what would Darwin have said about them? Darwin had three major ways of analyzing the appearance of characters that didn’t seem to be adaptive. One we saw in the last reading – he believed that, at least in some cases, the fact that an organism used a character extensively could alter its transmission to its offspring. That doesn’t work in this case, though. What would it mean to say that a particular elk used its antlers a lot?</p>
<p>Darwin’s second explanation involved what he called <em>sexual selection.</em> In order to have lots of offspring, not only do you have to survive and outperform the fellow members of your species, you also have to be successful at obtaining a mate. Darwin’s most common example of this process was, indeed, antlers. If males have to fight each other in order to be able to have offspring at all, then having weapons or shields for that fight, Darwin wrote, would be an obvious advantage, even if overall it might make it harder for the organism to live other parts of its life.</p>
<div class="think">
<p><strong>THINK[5]:</strong> Darwin’s way of thinking about sexual selection has been criticized for just recapitulating 19th-century gender roles, with angry, violent males that fight over the coy, choosy females. Is this reasonable, or just the illegitimate copying of social values into the scientific realm? Would there be a way to have a theory of sexual selection that didn’t have these problems, or not? And more generally, how can we be on the alert that we aren’t just interpreting our scientific data in the light of what seems reasonable or acceptable in our society?</p>
</div>
<p>But others argued that even this seems to be a problematic explanation, at least for something as exaggerated as Irish elk antlers. How exactly would those be useful, they argued? A massive weight on the end of a relatively weak neck wouldn’t be very useful for fighting. Antlers that are optimized for battle tend to be either light and sharp (think of <a href="https://www.youtube.com/watch?v=dg4VeesS6_I">the antlers of a deer</a>) or heavy, but not very large (<a href="https://www.youtube.com/watch?v=Ez7RUSCUhzk">like a ram</a>). These Irish elk antlers have the worst of both worlds, neither good for fighting nor good for defense.</p>
<p>Darwin had one more explanation to which he could appeal, then. He called this the <em>correlation of growth.</em> In many cases we don’t know exactly why this happens, he writes, but it seems as though some features of organisms are correlated or connected as the organism grows. As one feature gets larger, another might get larger along with it, or another feature might become smaller as it gets “crowded out.” It’s possible, then, that the large antlers of the Irish elk would be understandable in this way. Another important feature of Irish elk is their overall size – they were among the largest members of the deer family alive on earth at the time. Perhaps natural selection was favoring this! Being large is, of course, a great strategy to avoid predation. If getting larger and larger entailed a disproportionate increase in the size of antlers, then this might have driven what we see in the Irish elk.</p>
<div class="think">
<p><strong>THINK[6]:</strong> Can you think of problems with this explanation? If you were one of Darwin’s opponents, how could you criticize this?</p>
</div>
<p>Eimer, though, did have a compelling response. Why couldn’t natural selection break the correlation, if it was so important to build a larger deer with smaller antlers? To this, Darwinians could not reply. As we have already seen, because Darwin had no idea of genetics and the fundamentals of how the characters of organisms are actually constructed (about which more in the next reading), he didn’t know <em>how</em> the correlation of growth actually worked; he only knew that it was something that seemed to occur in many cases of agricultural breeding and variation that we can see in the wild.</p>
<div class="think">
<p><strong>THINK[7]:</strong> How much evidence, and of what kind, should we have in order to support a scientific theory? Both Eimer’s orthogenesis and Darwin’s correlation of growth appeal to processes that we don’t fully understand, and both believe that they have some empirical evidence to support those processes. How might you argue in favor of the correlation of growth, or orthogenesis, with only the <em>effects</em> of that process as your data?</p>
</div>
<p>Orthogenesis, on the other hand, could offer a clear explanation for why the antlers had become so oversized. Poor <em>Megaloceros</em> got stuck in a rut – the force of orthogenesis started to drive it in the direction of increased antler size, and the variation that was therefore available to the organism was only in the direction of increased antler size. At that point, the species was doomed! Its only possibility was to grow antlers larger and larger until it drove itself to extinction.</p>
<p>It’s important to see how attractive an explanation this could have been. Eimer spent most of his time looking for examples of orthogenesis as a way to explain living organisms, not only fossils (he had extensively studied butterflies, for instance), and the clean explanations that the “channeling” of variation could offer for non-adaptive trends in variation were hard for many other theorists to duplicate.</p>
<p>But as early developments in genetics began to unfold, there was little evidence that variation was actually constrained in this way. Orthogenesis fell apart in most areas of biology, though it lasted a little longer in paleontology, where there was less interest in (and less ability to study) the mechanisms of variation itself. We could explain most of the features of the biological world without having to add the extra “feature” of orthogenesis.</p>
<div class="think">
<p><strong>THINK[8]:</strong> We often see appeals to the simplicity of a scientific hypothesis, also called “Ockham’s razor” after the medieval philosopher William of Ockham (1285–1347). Simpler scientific hypotheses are thus taken to better match the world. Do you think this is a good principle for scientific reasoning? What kinds of problems might it lead to, and how could we avoid them?</p>
</div>
<p>What about our Irish elk, then? As contemporary biology advanced, a modified version of Darwin’s correlation of growth thesis emerged. The contemporary <a href="https://en.wikipedia.org/wiki/Allometry">theory of <em>allometry</em></a> investigates precisely these correlations – and makes room for the idea that these correlated parts could grow at different rates. As the famous biologist Stephen Jay Gould (1941–2002) described it in the 1970s:</p>
<div class="quote-wrapper">
<div class="quote-title">
<p>Gould, from the journal <em>Evolution</em> (1974)</p>
</div>
<div class="quote" title="Gould, from the journal \emph{Evolution} (1974)">
<p>The misnamed “Irish Elk” is a late Pleistocene giant deer that ranged southward to North Africa and eastward to China. Since its first scientific description in 1697, it has played a major role in debates about the history of life. Cuvier used it to prove the fact of extinction and set the basis for a geologic time scale. Later, <em>Megaloceros</em> became the rallying point for anti-Darwinians; they invoked orthogenesis to deny natural selection and attributed extinction to an inadaptive trend towards immense antlers. The antlers posed a severe difficulty for the modern synthesis: they were generally explained as allometric correlates of advantageous increases in body size that offset the problems of admittedly disproportionate antlers. Virtually every textbook in evolution cites <em>Megaloceros</em> as a case of allometry <em>contra</em> orthogenesis: nonetheless, no one has ever generated any quantitative data about it.<a href="#fn5" class="footnote-ref" id="fnref5" role="doc-noteref"><sup>5</sup></a></p>
</div>
</div>
<p>By the 1970s, then, most biologists were convinced that something like the allometry hypothesis would be the correct explanation of the evolution of Irish elk antlers. But we still didn’t have any hard data about them! We need to clearly, empirically demonstrate that this kind of correlation would actually exist, <em>and</em> we would need to demonstrate that the antlers weren’t being selected themselves – that is, that it really was selection for large body size that was doing all of the work.</p>
<div class="think">
<p><strong>THINK[9]:</strong> Gould is implicitly considering the following three alternatives for how this evolutionary change might have occurred. First, it might be that the antlers were evolving independently of body size – that is, that antlers and size aren’t actually correlated at all. If they are, there are two further possible explanations. It could be that while both body size and antler size were going up, natural selection was responsible for both. Or, lastly, it could be that natural selection was <em>only</em> responsible for the increase in body size, and the antler size increase was entirely “by accident.”</p>
<p>What kind of data would you collect in order to tell the difference between these three hypotheses? How would you know that you had proven one and disproven the others?</p>
</div>
<p>Gould first set about doing the measurements, and he did indeed find positive correlation. But, he also noted that there’s no apparent reason why natural selection couldn’t have created these characters, too:</p>
<div class="quote-wrapper">
<div class="quote-title">
<p>Gould, from the journal <em>Evolution</em> (1974)</p>
</div>
<div class="quote" title="Gould, from the journal \emph{Evolution} (1974)">
<p>I measured 79 skulls and antlers of <em>Megaloceros</em> to resolve two questions… If selection acted to preserve deer of large body size, relatively large antlers would follow as a consequence of correlation.</p>
<p>Yet the fact of positive allometry need not provoke the usual interpretation: large bodies might be a consequence of advantageous antlers, or both antlers and bodies might be selected in concert. The assumption of disproportionate antlers is based on the <em>a priori</em> notion that antlers must function as weapons in battle…. But deer often use their antlers to establish dominance and win access to females by display and ritualized combat… alone among deer with palmated antlers, <em>Megaloceros</em> displayed its full palm when simply looking straight ahead.</p>
<p>The immense antlers of <em>Megaloceros</em> were advantageous in themselves. Its extinction may be traced to late glacial changes in climate.<a href="#fn6" class="footnote-ref" id="fnref6" role="doc-noteref"><sup>6</sup></a></p>
</div>
</div>
<figure>
<img src="/images/lessons/peacock.jpg" class="smallimg" style="width:33.0%" alt="A peacock, being bad at escaping from predators (CC-BY-SA; by BS Thurner Hof at Wikimedia Commons)" /><figcaption aria-hidden="true">A peacock, being bad at escaping from predators (CC-BY-SA; by BS Thurner Hof at Wikimedia Commons)</figcaption>
</figure>
<p>Gould wants us to ask the following question: why did we assume that these antlers couldn’t be adaptive? Recall that when we discussed the usefulness of these antlers, we assumed at first that they would be for fighting. Since they would have been bad weapons for attack, and bad shields for defense, they must not have been adaptations. But that’s not the only thing they could have been for. Another example of sexual selection, one that Darwin had also used, was peacock feathers. The tails of peacocks make it much easier for them to be caught by predators, but if displaying them makes them more attractive to peahens, then again, the cost might be worth it. Maybe Irish elk’s antlers were good as ornamentation, to impress potential mates. Or perhaps the combat isn’t about real <em>combat,</em> but rather about ritual, where deer engage in mock battles, in which the animal with the smaller horns, because it already knows it’s going to lose, eventually submits to defeat. Both of these seem like very plausible explanations that invoke only natural selection. And then the extinction of <em>Megaloceros</em> doesn’t need any kind of special interpretation. It was just a change in climate, as the glaciers that had covered much of their range retreated, that caused them to go extinct.</p>
<p>From the late-nineteenth to the mid-twentieth century, then, there was a whole variety of explanations for how the antlers of the Irish elk might have evolved:</p>
<ul>
<li>orthogenesis, or the limitation of available variations pushing in the direction of large antlers</li>
<li>correlation of growth or allometry, or selection operating on one part, but influencing another part</li>
<li>sexual selection, the use of antlers to attract mates and dissuade rivals</li>
</ul>
<p>Deciding between these wasn’t easy, and it depended on the evidence available at the time. Orthogenesis was a really natural response to seeing trends like this, both in the fossil record and in living species, and it was only when the evidence for it began to conflict with the early science of genetics that it was abandoned.</p>
<p>But we should step back for a moment and ask another question. Why was this problem so important for so many biologists? What about <em>non-adaptive</em> characters was so compelling that (as we saw in this and the last reading) many biologists wanted theories like neo-Lamarckism or orthogenesis to explain them? One answer to this question would point to the impact of Weismann’s work, as we already saw here – if someone is arguing very explicitly that <em>every</em> trait is the result of natural selection, then looking at non-adaptive characters would be a very obvious way to respond.</p>
<p>Another feature, to which many authors have pointed, has to do with the <em>social</em> consequences of evolutionary theory.<a href="#fn7" class="footnote-ref" id="fnref7" role="doc-noteref"><sup>7</sup></a> Even by Eimer’s day, some authors were beginning to read Darwin as a way to defend <a href="https://eugenicsarchive.ca/discover/connections/535eee377095aa0000000259">“survival of the fittest” in making social policy</a> – supporting the idea that the “weak” should be allowed to die as a way to benefit the “strong.” Darwin himself did not believe this, but turning to a force like orthogenesis, which proposed that species could change without involving struggle and fitness, would permanently leave room for a less violent approach to evolutionary change. Some of these authors – Eimer included – even had philosophical commitments that made them think that the world <em>should</em> exhibit orderly, directional behavior like what orthogenesis proposed, not the kind of branching complexity that evolutionary theory implied.</p>
<div class="think">
<p><strong>THINK[10]:</strong> What do you think is the importance of scientific theory for social or governmental policy? That is, what kinds of discoveries in science might be important for the ways in which we structure our social life? How could science be abused when it makes the jump to policy? What responsibilities do scientists have if they think their work might be used in this way?</p>
</div>
<p>We can see, then, that investigating orthogenesis is a perfect way to understand some of the concerns of late-nineteenth and early-twentieth century biologists. These weren’t just scientific concerns, either – some were, but others included philosophical and social commitments. Scientific disputes are often about much more than just the science, and the debate over non-adaptive variation is no different!</p>
<h2 id="think-nos-reflection-questions">THINK: NOS Reflection Questions</h2>
<p>What does the story of orthogenesis tell us about the following features of the nature of science?</p>
<ul>
<li>alternative explanations</li>
<li>evidential relevance (empiricism)</li>
<li>social responsibility of scientists</li>
<li>conceptual change</li>
<li>role of gender bias</li>
<li>uncertainty</li>
<li>role of systematic study (vs. anecdote)</li>
</ul>
<h2 id="further-reading">Further Reading</h2>
<ul>
<li>Bowler, Peter J. 1992. <a href="https://www.worldcat.org/title/eclipse-of-darwinism-anti-darwinian-evolution-theories-in-the-decades-around-1900/oclc/26547189"><em>The Eclipse of Darwinism: Anti-Darwinian Evolution Theories in the Decades around 1900.</em></a> Baltimore, MD: Johns Hopkins University Press.</li>
<li>Bowler, Peter J. 1979. “Theodor Eimer and Orthogenesis: Evolution by ‘Definitely Directed Variation.’” <em>Journal of the History of Medicine and Allied Sciences</em> XXXIV (1): 40–73. <a href="https://doi.org/10.1093/jhmas/XXXIV.1.40" class="uri">https://doi.org/10.1093/jhmas/XXXIV.1.40</a>.</li>
<li>Ulett, Mark A. 2014. “Making the Case for Orthogenesis: The Popularization of Definitely Directed Evolution (1890–1926).” <em>Studies in History and Philosophy of Biological and Biomedical Sciences</em> 45: 124–32. <a href="https://doi.org/10.1016/j.shpsc.2013.11.009" class="uri">https://doi.org/10.1016/j.shpsc.2013.11.009</a>.</li>
</ul>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>Weismann, August. 1891 [1883]. <a href="https://doi.org/10.5962/bhl.title.28066"><em>Essays upon Heredity and Kindred Biological Problems.</em></a> Edited by Edward B. Poulton, Selmar Schönland, and Arthur E. Shipley. 2nd ed. Oxford: Clarendon Press, p. 138.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2" role="doc-endnote"><p>Eimer, Theodor. 1898. <a href="https://doi.org/10.5962/bhl.title.87978"><em>On Orthogenesis: And the Impotence of Natural Selection in Species-Formation.</em></a> Translated by Thomas J. McCormack. Chicago: Open Court, p. 2.<a href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn3" role="doc-endnote"><p>Eimer, p. 21.<a href="#fnref3" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn4" role="doc-endnote"><p>Eimer, p. 21.<a href="#fnref4" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn5" role="doc-endnote"><p>Gould, Stephen Jay. 1974. “The Origin and Function of ‘Bizarre’ Structures: Antler Size and Skull Size in the ‘Irish Elk,’ <em>Megaloceros Giganteus.</em>” <em>Evolution</em> 28 (2): 191–220, p. 216. <a href="https://doi.org/10.1111/j.1558-5646.1974.tb00740.x" class="uri">https://doi.org/10.1111/j.1558-5646.1974.tb00740.x</a>.<a href="#fnref5" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn6" role="doc-endnote"><p>Gould, p. 217.<a href="#fnref6" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn7" role="doc-endnote"><p>Bowler, Peter J. 1979. “Theodor Eimer and Orthogenesis: Evolution by ‘Definitely Directed Variation.’” <em>Journal of the History of Medicine and Allied Sciences</em> XXXIV (1): 40–73. <a href="https://doi.org/10.1093/jhmas/XXXIV.1.40" class="uri">https://doi.org/10.1093/jhmas/XXXIV.1.40</a>.<a href="#fnref7" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
